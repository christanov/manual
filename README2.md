# Documentación de la REST API de Facturación Digital 

En este documento se describe la REST API de Facturación Digital, que permite a los desarrolladores interactuar con la creación, anulación, busqueda y distribución de documentos fiscales. Aquí encontrarás información detallada sobre los recursos disponibles, los métodos admitidos, los parámetros de solicitud y respuesta, y más.
***

- [Documentación de la REST API de Facturación Digital](#documentación-de-la-rest-api-de-facturación-digital)
  - [Información de contacto y metadatos](#información-de-contacto-y-metadatos)
  - [Confidencialidad](#confidencialidad)
  - [Aspectos Generales](#aspectos-generales)
    - [Documentos fiscales soportados (tipos de documento)](#documentos-fiscales-soportados-tipos-de-documento)
    - [Numeros de documento](#numeros-de-documento)
    - [Numeros de Control](#numeros-de-control)
    - [Talonarios digitales](#talonarios-digitales)
    - [Series de facturación](#series-de-facturación)
      - [Notas](#notas)
  - [Facturación multimoneda](#facturación-multimoneda)
  - [Facturación en linea](#facturación-en-linea)
  - [Facturación por lotes](#facturación-por-lotes)
  - [Asignación de Numeros de Control](#asignación-de-numeros-de-control)
    - [Por la Imprenta](#por-la-imprenta)
    - [Por la Empresa (Contribuyente)](#por-la-empresa-contribuyente)
  - [Aspectos Técnicos](#aspectos-técnicos)
    - [¿Por qué un REST API?](#por-qué-un-rest-api)
    - [Solicitud de creación de sandbox](#solicitud-de-creación-de-sandbox)
    - [Seguridad](#seguridad)
      - [Autenticación](#autenticación)
      - [Llaves publicas y privadas](#llaves-publicas-y-privadas)
    - [Codigos HTTP de respuesta](#codigos-http-de-respuesta)
    - [Entidades principales del API](#entidades-principales-del-api)
      - [Documento (Document)](#documento-document)
      - [Ciclo o Lote (Batch)](#ciclo-o-lote-batch)
  - [Operaciones básicas](#operaciones-básicas)
    - [Iniciar sesión](#iniciar-sesión)
      - [Respuesta exitosa](#respuesta-exitosa)
    - [Listar las series asignadas a la empresa](#listar-las-series-asignadas-a-la-empresa)
      - [Respuesta exitosa](#respuesta-exitosa-1)
    - [Listar los tipos de documentos](#listar-los-tipos-de-documentos)
      - [Respuesta exitosa](#respuesta-exitosa-2)
    - [Creación de Talonarios digitales](#creación-de-talonarios-digitales)
      - [Respuesta exitosa](#respuesta-exitosa-3)
      - [Significado de cada campo del JSON](#significado-de-cada-campo-del-json)
      - [Algortimo para validar Hash](#algortimo-para-validar-hash)
    - [](#)
  - [Creación de documentos](#creación-de-documentos)
    - [En línea (solo un documento)](#en-línea-solo-un-documento)
    - [Por lotes (hasta 500 documentos por solicitud)](#por-lotes-hasta-500-documentos-por-solicitud)
      - [Notas](#notas-1)
      - [Paso 1: Abrir ciclo](#paso-1-abrir-ciclo)
      - [Paso 2: Agregar documentos al ciclo](#paso-2-agregar-documentos-al-ciclo)
      - [Paso 3: Cerrar ciclo](#paso-3-cerrar-ciclo)
      - [Aprobar ciclo](#aprobar-ciclo)
      - [Cancelar ciclo](#cancelar-ciclo)
        - [Campos obligatorios](#campos-obligatorios)
    - [En línea y Pre-asignado](#en-línea-y-pre-asignado)
  - [Otras operaciones](#otras-operaciones)
    - [Ver información de un documento asignado](#ver-información-de-un-documento-asignado)
      - [Respuesta exitosa](#respuesta-exitosa-4)
  - [Tutoriales](#tutoriales)
    - [Crear documento en dólares con conversión en bolívares](#crear-documento-en-dólares-con-conversión-en-bolívares)
    - [Crear documento en bolívares con conversión en dólares](#crear-documento-en-bolívares-con-conversión-en-dólares)
    - [Buscar documento por numero de control](#buscar-documento-por-numero-de-control)
      - [Respuesta exitosa](#respuesta-exitosa-5)
    - [Buscar documento por numero de documento y serie](#buscar-documento-por-numero-de-documento-y-serie)
      - [Respuesta exitosa](#respuesta-exitosa-6)
    - [Visualizar un documento por URL](#visualizar-un-documento-por-url)
    - [Obtener el stream en PDF de un documento](#obtener-el-stream-en-pdf-de-un-documento)
 

## Información de contacto y metadatos

- **Empresa:** [Corporación Unidigital 1220 C.A](https://www.unidigital.global)
- **Propietario de la API:** [Corporación Unidigital 1220 C.A](https://www.unidigital.global)
- **Creador de la API:** [Carlos Luis Salazar Ibarra](mailto:carlos.salazar@unidigital.global)
- **Sitio web:** https://www.unidigital.global
- **Soporte del API:** [api@unidigital.global](mailto:api@unidigital.global)
- **URL ambiente de producción:** https://www.unidigital.global/digitalinvoiceservice/api
- **URL ambiente de pruebas (demo):** https://demo.unidigital.global:7683//digitalinvoiceservice/api

## Confidencialidad

Este documento contiene información confidencial y solo puede ser accedido por personas autorizadas que representen a Empresas que tengan a Corporación Unidigital 1220 como su Imprenta Digital Autorizada ante el SENIAT. Cualquier divulgación no autorizada está estrictamente prohibida.

Para acceder a la información contenida en este documento, debe estar autorizado por la Empresa y haber recibido un permiso explícito para acceder a esta REST API.
***

## Aspectos Generales

### Documentos fiscales soportados (tipos de documento)

Actualmente el Sistema soporta solo Facturas, Notas de Credito y Notas de Debito, estamos planeando agregar nuevos tipos de documentos.

### Numeros de documento

Cada documento emitido por la Empresa debe tener una numeración unica por tipo de documento asi tenemos que por ejemplo si emitimos 3 facturas, 2 notas de credito y 1 notas de debito, todos por primera vez las numeraciones quedarian de la siguiente forma:

| Tipo | Fecha | Numero |
| ----------- | ----------- | ----------- |
| Factura | 01/01/2023 | 1
| Factura | 01/01/2023 | 2
| Factura | 01/01/2023 | 3
| Nota de Crédito | 01/01/2023 | 1
| Nota de Crédito | 01/01/2023 | 2
| Nota de Débito | 01/01/2023 | 1

Como verán en la tabla anterior cada tipo de documento tiene su propia numeración, es decir su propio correlativo que en la medida que se emite el documento debe incrementarse el número.

Esta responsabilidad de manejar la numeración del documento por ahora queda de parte de la Empresa y no de la Imprenta Digital. 

### Numeros de Control

El número de control es un entero generado por la Imprenta Digital y asignado a cada Documento fiscal emitido por la Empresa que contrata los servicios de facturación digital. Este número es unico e irrepetible independientemente del tipo de documento emitido.

Continuando con el ejemplo de Numeros de documento

| Tipo | Fecha | Numero | Numero de Control
| ----------- | ----------- | ----------- | ----------- |
| Factura | 01/01/2023 | 1 | 1 
| Factura | 01/01/2023 | 2 | 2
| Factura | 01/01/2023 | 3 | 3
| Nota de Crédito | 01/01/2023 | 1 | 4
| Nota de Crédito | 01/01/2023 | 2 | 5
| Nota de Débito | 01/01/2023 | 1 | 6

Como verán el numero de control es un correlativo independiente del número de documento.

### Talonarios digitales

La imprenta para gestionar los números de control utiliza la figura de talonarios digitales, los cuales agrupan una cantidad de numeros de control que sirven generalmente para cubrir el consumo de un mes de facturación.

Los talonarios digitales son la representación digital de los talonarios de formas libres que comunmente se utilizan con las imprentas tradicionales. Todo talonario tiene una fecha de creación, un rango de numeros de control y una serie asociada.

Los talonarios estan asociados a una serie, como veremos mas adelante toda Empresa tiene al menos una serie asociada que llamaremos serie en blanco o serie 0 (cero) las cuales representan un nivel de agrupación para los numeros de documentos.

### Series de facturación

Cuando una empresa tiene varios sistemas, o varias lineas de negocio, surge la necesidad de crear series de facturación. Las series de facturación permiten asociar una cantidad de numeros de control con una serie, por defecto al registrar la empresa dentro de la plataforma una serie en blanco es asociada automaticamente, de manera que todos los numeros de control creados se haran bajo esa serie. 

Si la empresa necesita tener mas de una serie, entonces la nueva serie se iniciara con la letra A.

Para la creación de series debe plantear su caso al departamento Legal de Unidigital y luego de la aprobación sera creada la serie dentro del sistema.

Continuando con el ejemplo para una Empresa que tiene la serie por defecto quedaria asi

| Tipo | Fecha | Numero | Numero de Control | Serie
| ----------- | ----------- | ----------- | ----------- | ----------- |
| Factura | 01/01/2023 | 1 | 1 | 0
| Factura | 01/01/2023 | 2 | 2 | 0
| Factura | 01/01/2023 | 3 | 3 | 0
| Nota de Crédito | 01/01/2023 | 1 | 4 | 0
| Nota de Crédito | 01/01/2023 | 2 | 5 | 0
| Nota de Débito | 01/01/2023 | 1 | 6 | 0

Como verán todos los documentos pertenecen a la Serie 0 (Cero) como le llamamos en Unidigital. La Serie 0 es la serie por defecto que se crea para todas las Empresas.

En la sección de talonarios digitales comentamos que todo talonario esta asociado a una serie, esto quiere decir que si la Empresa tiene mas de una serie entonces un talonario digital al ser creado debera especificarse para que serie se necesita.

Supongamos que tenemos dos talonarios digitales, cada uno de 10 numeros de control, uno asociado para la serie 0 y otro para la serie A. Los talonarios se verian asi:

| Serie | Numero de Control Desde | Numero de Control Hasta | 
| ----------- | ----------- | ----------- | 
| 0 | 1 | 10 | 
| A | 11| 20 | 

Estos talonarios pueden ser usados en paralelo, y la creación de la serie permite que tengamos nuevos consecutivos para los tipos de documentos. Usemos un ejemplo para entender.

Ahora que tenemos dos talonarios una para la serie 0 y otro para la serie A, queremos crear 3 facturas para la serie 0, 2 notas de debito para la serie 0, 5 facturas para la serie A y 5 notas de credito para la serie A, ¿Como quedarian los numeros de documentos y numeros de control? Observemos:


| Tipo | Fecha | Numero de Documento | Numero de Control | Serie
| ----------- | ----------- | ----------- | ----------- | ----------- |
| Factura | 01/01/2023 | 1 | 1 | 0
| Factura | 01/01/2023 | 2 | 2 | 0
| Factura | 01/01/2023 | 3 | 3 | 0
| Nota de Débito | 01/01/2023 | 1 | 4 | 0
| Nota de Débito | 01/01/2023 | 2 | 5 | 0
| Factura | 01/01/2023 | 1 | 11 | A
| Factura | 01/01/2023 | 2 | 12 | A
| Factura | 01/01/2023 | 3 | 13 | A
| Factura | 01/01/2023 | 4 | 14 | A
| Factura | 01/01/2023 | 5 | 15 | A
| Nota de Crédito | 01/01/2023 | 1 | 16 | A
| Nota de Crédito | 01/01/2023 | 2 | 17 | A
| Nota de Crédito | 01/01/2023 | 3 | 18 | A
| Nota de Crédito | 01/01/2023 | 4 | 19 | A
| Nota de Crédito | 01/01/2023 | 5 | 20 | A

Si observan con detalle, la numeración de los documentos por serie y por tipo de documento es independiente pudiendo entonces tener una factura con numero 1 para la serie 0 y tambien en la serie A. Adicionalmente los numeros de control se van usando en la medida que son requeridos por la serie, sin embargo estos nunca se repiten.

#### Notas

* En la mayoria de los casos una Serie sera suficiente para cubrir las necesidades de las empresas.

## Facturación multimoneda

Actualmente el REST API soporta las siguientes monedas como principales:

1. Bolivares (VES)
2. Dolares americanos (USD)
3. Euros (EUR)

Un documento fiscal siempre tendra una moneda principal en el cual fue emitido, y como regla general si esta moneda es diferente de Bolivares entonces se debe mostar en algún lugar del documento la tasa de cambio oficial utilizada y la conversión en Bolívares.

Es decir, si emites una Factura en Dolares americanos debes indicar la tasa de cambio del [Banco Central de Venezuela](https://www.bcv.org.ve/) y el resultante equivalente en Bolivares.

## Facturación en linea

La facturación en línea es un modelo de trabajo que se basa en mantener casi en tiempo real los Sistemas sincronizados, es decir, que cuando el documento sea emitido en su Sistema ERP deberá ser inmediatamente creado dentro del ecosistema de Unidigital.

Este modelo aplica muy bien para empresas que necesiten emitir y hacer llegar la factura lo antes posible al cliente final.

## Facturación por lotes

La facturación por lotes es un model de trabajo que se basa en acumular los documentos emitidos del lado del Sistema de la Empresa y llegada una cantidad de documentos acumulados o una fecha determinada enviar de forma masiva o por lotes todos los documentos hacia Unidigital.

Este escenario es el preferido por empresas de servicios de cobro recurrente y mensual, caso empresas que venden servicio de internet.

La facturación por lotes usa como base la facturación en línea pero altamente optimizada para utilizar de mejor forma las herramientas técnicas disponibles.

## Asignación de Numeros de Control

### Por la Imprenta

En la mayoría de los casos la Empresa "Emite" los documentos y son enviados a la Imprenta para que esta realice un proceso de asignación de numeros de control a cada documento. En este caso la responsabilidad de asignar los numeros de control de manera consecutiva es de la Imprenta. Este escenario es el mas común y el que conlleva menos complejidad técnica y por defecto el API trabaja bajo esta modalidad.

### Por la Empresa (Contribuyente)

Existen casos especiales donde la Imprenta ofrece a la Empresa un Talonario con los rangos de numeros de control y es la empresa quien debe asignar los numeros de control, y notificar a la imprenta a que documentos fueron asignados los numeros de control de manera de garantizar que el documento es 100% fiscal.

¿En que escenario esto es común?
Cuando la Empresa ya posee un Sistema Corporativo que puede garantizar el respaldo durante seis años segun exige la providencia y donde la empresa tiene un diseño de factura ya establecido al cual solo desea agregarle el numero de control. 

*** 
## Aspectos Técnicos

### ¿Por qué un REST API?

En Unidigital, Imprenta Digital autorizada por el SENIAT hemos decidido ofrecer nuestros servicios a través de un REST API que permita a todos nuestros clientes construir experiencias basadas en tecnologías interconectadas donde los diferentes Sistemas se puedan comunicar de manera transparente y eficiente.

Este documento te ayudará a interactuar con nuestra REST API para que puedas conectar tu ERP, tu Sitio Web o Aplicación Móvil y así ofrecerle a tu cliente una mejor experiencia al administrar sus documentos fiscales.

En la construcción de nuestra REST API valoramos mucho las sugerencias de los desarrolladores que la utilizan, si tienes alguna puedes escribirnos a <api@unidigital.global>


### Solicitud de creación de sandbox

Lo primero que necesitas para usar el REST API es tener unas credenciales asociadas a la Empresa que representas para conectarte a nuestro SandBox, si no tienes credenciales puedes solicitarla enviando un correo a <api@unidigital.global> indicando tu nombre, apellido, numero de cedula, y los datos fiscales de la empresa que representas. Una vez validada la información recibirás en tu correo las credenciales.

### Seguridad

#### Autenticación

La seguridad de la REST API esta basada en JWT. Para mas información puedes visitar [JSON Web Token - Wikipedia, la enciclopedia libre](https://en.wikipedia.org/wiki/JSON_Web_Token) y la documentación oficial [JSON Web Tokens - jwt.io](https://jwt.io/).

Enviando las credenciales recibiras un token el cual deberas anexar a cada una de las peticiones que realices posteriormente.

#### Llaves publicas y privadas

Para garantizar la autenticidad, integridad y no repudio, tanto la Empresa como la Imprenta deben generar e intercambiar unas llaves públicas utilizando el algortimo RSA.

Para el caso de la Empresa se recomienda utilizar [OpenSSL](https://www.openssl.org/) y ejecutar los siguientes comandos:

```
//Generación de llave privada con un tamaño de 2048 bits
//Se creara un archivo private_key_contribuyente.pem
//Asegurese de no perder esta llave y guardarla con mucha responsabilidad

openssl genrsa -out private_key_contribuyente.pem 2048

//Generación de llave publica
//Esta llave publica debera compartirla con la Imprenta 

openssl rsa -in private_key_contribuyente.pem -pubout -out public_key_contribuyente.pem

```

Una vez generada la llave pública deberá enviar el archivo public_key_contribuyente.pem a la Imprenta a [api@unidigital.global](mailto:api@unidigital.global) para que sea configurada. A su vez la Imprenta deberá compartir con la Empresa la llave pública la cual puede ser solicitada al mismo correo.

Ambos entes deben compartir sus llaves públicas ya que es necesario para validar algunas operaciones del API.

### Codigos HTTP de respuesta

Las respuestas del API estan asociados a los estados HTTP

- **200 OK**: El servidor ha procesado la solicitud correctamente y ha retornado una respuesta exitosa. 
- **400 Bad Request**: La solicitud no se pudo entender o estaba incompleta. Esta respuesta generalmente se asocia con un error en los datos enviados al servidor, algún campo requerido que no fue especificado o el incumplimiento de alguna regla de negocio como puede ser enviar un numero de documento repetido.
- **401 Unauthorized**: La solicitud requiere autenticación, pero el cliente no ha proporcionado credenciales válidas. Generalmente se debe a que el token no ha sido especificado en el header de la solicitud o simplemente que el token esta vencido.
- **500 Internal Server Error**: Se ha producido un error en el servidor mientras procesaba la solicitud. Estos errores son los menos comunes, es importante que del lado del cliente sean logeados y notificados al equipo de Unidigital para su evaluación y correción inmediata.

Notas: para las respuestas con HTTP Status Code 400 el json de respuesta siempre tendra la siguiente estructura

```javascript
{
    "result": null,
    "errors": [
        {
            "code": "0000",
            "message": "Mensaje explicativo del error",
            "extra": null
        }
    ],
    "success": [],
    "information": [],
    "haserrors": true
}
```

El campo errors es un arreglo que contiene una lista de todos los errores que fueron detectados durante el procesamiento de la solicitud, se recomienda que estos errores siempre sean logeados del lado del consumidor del API.

### Entidades principales del API

#### Documento (Document)

Representa un Documento generado por el Sistema Corporativo del Contribuyente, por ejemplo una Factura, Nota de Crédito o Nota de Débito. Todo documento siempre formará parte de un ciclo. Si el documento es creado bajo el concepto de "Facturación en Linea" entonces se creará un ciclo por cada documento transmitido al API, por el contrario si el documento es creado bajo el concepto de "Facturación por Lotes" entonces se creará un ciclo que podra almacenar N cantidad de documentos.

#### Ciclo o Lote (Batch)

El Ciclo o Lote representa un elemento que permite agrupar uno o muchos documentos y contiene información valiosa como puede ser el CurrentStatus (Estatus actual), fecha de creación, fecha de asignación de documentos, etc. 

Tabla de Estaus 

| Estatus | Valor | Comentario
| ------- | ----- | ----------
| Open | 1 | Abierto, puede agregar documentos
| Closed | 2 | Cerrado, no puede recibir documentos 
| Approved | 3 | Aprobado, en espera de asignación de números de control
| Assigned | 4 | Asignado, ya los documentos le fueron asignados los numeros de control y quedan como documentos fiscales
| Canceled | 5 | Cancelado, los documentos del Ciclo seran eliminados, nunca seran documentos fiscales  





***
## Operaciones básicas

### Iniciar sesión

Para iniciar sesión, debes tener usuario y contraseña asignada a la Empresa que representas

POST /user/login

```javascript
{
    "username": "user123@thedomain.com",
    "password": "secretpassword"
}
```

#### Respuesta exitosa

```javascript
{
    "userName": "user123@thedomain.com",
    "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InVzZXIxMjNAdGhlZG9tYWluLmNvbSIsIm5iZiI6MTY4NDEwNTUyMiwiZXhwIjoxNjg0MTM0MzIyLCJpYXQiOjE2ODQxMDU1MjIsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0OjMwMDAvIiwiYXVkIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6MzAwMC8ifQ.zajWSme-1gThgbgsmsQpWmEU7QTlhJGfqajIpGatqHA",
    "refreshToken": ""
}
```

### Listar las series asignadas a la empresa

Para listar todas las series que tiene disponible la empresa. Una vez creadas las series en la plataforma de la imprenta, los strongId no variaran en el tiempo, son fijos.

POST /series/list

* El Body se envia vacío.

#### Respuesta exitosa

```javascript
{
    "result": [
        {
            "strongId": "a1b9deb6-11a9-4e4f-8d33-a349db640d8c",
            "name": "A"
        }
    ],
    "errors": [],
    "success": [],
    "information": [],
    "haserrors": false
}
```

### Listar los tipos de documentos

Para listar todos los tipos de documentos que soporta el Sistema

POST /documenttypes/list

* El Body se envia vacío

#### Respuesta exitosa

```javascript
{
    "result": [
        {
            "strongId": "c8d3e058-fe5a-4d38-853e-834ee49ba9fb",
            "name": "Factura",
            "codeName": "FA"
        },
        {
            "strongId": "dbeda099-e4e5-46ae-8ce4-3271c3182fb9",
            "name": "Nota de Crédito",
            "codeName": "NC"
        },
        {
            "strongId": "dd2ff34e-8fc7-48f8-99d2-73d41e5b4499",
            "name": "Nota de Débito",
            "codeName": "ND"
        },
        {
            "strongId": "cdf97d1e-b8e9-490d-bc0c-251b131b92ea",
            "name": "Ninguno",
            "codeName": "NN"
        }
    ],
    "errors": [],
    "success": [],
    "information": [],
    "haserrors": false
}
```

### Creación de Talonarios digitales

Para el caso donde la Empresa es quien manejará la asignación de los numeros de control deberá solicitar la creación de los talonarios digitales con la finalidad de habilitar un rango dentro del API.

Una vez creado el talonario, la Empresa podra "Asignar" los numeros de control a sus documentos en su propio Sistema y luego realizar la notificación al API de a cual documento fue asignado el número de control.

Importante: si no realiza la notificación al API, el documento no sera reconocido por la imprenta y por lo tanto no tendrá validez fiscal.

Para crear un talonario digital 


POST /controlNumbers/book?serieStrongId=1730ad9a-4205-4264-a961-2f26115e555d&quantity=10000

En el query string debera indicar el valor de dos parametros:

| Nombre | Descripción | 
| ----------- | ----------- | 
| serieStrongId | StrongId de la serie para la cual necesita un talonario | 
| quantity | Cantidad de numeros de control que desea  |

* El Body se envia vacío

#### Respuesta exitosa

```javascript
{
    "result": {
        "strongId": "ceec9b06-5ea7-45fa-93d8-9040dfe38f79",
        "serieStrongId": "1730ad9a-4205-4264-a961-2f26115e555d",
        "minNumber": 6001,
        "maxNumber": 16000,
        "total": 10000,
        "factoryDate": "2023-07-24T13:39:55.8906781-04:00",
        "encryptedHashCompany": "FuuGxZId/ya1FM4GKp3h11SnyBJGfW3/LcWPugapxSFCB1/bZQ4TnNVLyoWxIq/Gaud9rweKCp0dwpOdCrDrEgLwKhZiuzBX+G3L08FVcT1AbNLFl/aZrwmKgC7MittBspjYR4owv/BhtY20tuSvmw9MedbM/I2C1v0wspMTk3lZOG6Fftd4Hq+Lp+8wCDh4ftjHjdWumutbWhn258W7luVlpBmoMoKUnwyeEqBx+ylF+bHn4DitgXcaY1RyCfsoIwcPcO8uSTtrtkxWWnwmgfBm325Ydf8iYST6wO/x0Gy2ch4bfXc5hsm2LtMLyq4AI7jycVfYKtJai9o/nywupw==",
        "encryptedHashPrintingHouse": "Cv/OGD96ZpaZOe53FCkF1qHiMZI84MpDTxhBbqzRzAUA1p9JKPY33qcwhDmwPKtAu0bQ9P6cwAQ1J0O4Kg6fzc9QrM1LUo+sPmdN4+OyQN3RE4q+M/ljg0Y7Ta+KV+b/O2mSRs4qymZmZk5J9YaOwDyOCxw9iDMYzOazwFFEgqU="
    },
    "errors": [],
    "success": [],
    "information": [],
    "haserrors": false
}
```
#### Significado de cada campo del JSON

| Nombre | Descripción | 
| ----------- | ----------- | 
| strongId | Identificador tipo GUID del talonario | 
| serieStrongId | Identificado tipo GUID de la serie |
| minNumber | Inicio del rango de número de control |
| maxNumber | Fin del rango de número de control |
| total | Total de números de control permitidos |
| factoryDate | Fecha de creación |
| encryptedHashCompany | Hash cifrado que debe ser descifrado con la llave privada del contribuyente |
| encryptedHashPrintingHouse | Hash cifrado que debe ser descifrado con la llave pública de la imprenta |

#### Algortimo para validar Hash 

La empresa debe validar el talonario comparando los hash para ello debe seguir el siguiente algortimo:

```code
//C#

var x = strongId.ToString();
x+= serieStrongId.ToString();
x+= minNumber.ToString();
x+= maxNumber.ToString();
x+= total.ToString();
x+= factoryDate.ToString("yyyyMMddHHmmss");

var hash = funcion_sha_256(x);
var descifrado_empresa = funcion_descifrar_con_rsa(encryptedHashCompany, llave_privada_empresa)
var descifrado_imprenta = funcion_descifrar_con_rsa(encryptedHashPrintingHouse, llave_publica_imprenta)

if (!(hash == descifrado_empresa && hash == descifrado_imprenta)) {
    Console.Writeline("Los hash coinciden, el talonario esta correcto");
} else {
    Console.Writeline("Los hash no coinciden, el talonario es incorrecto");
}

```

###  

***
## Creación de documentos

### En línea (solo un documento)

La creación de un documento en línea implica la creación de un ciclo, agregar un documento e inmediatamente aprobarlo para que quede disponible para la asignación de números de control. Este método simula en una sola llamada la creación del ciclo, agregar documento, cerrar el ciclo y aprobarlo. Por eso es altamente recomendable para facturación en vivo o en línea.

POST /documents/createandapprove

```javascript
{
    "SerieStrongId": "a1b9deb6-11a9-4e4f-8d33-a349db640d8c",
    "DocumentType": "ND",
    "Number": 99875427,
    "EmissionDateAndTime": "7/6/2022 15:28",

    //Datos del contribuyente a quien se le crea el documento 
    "Name": "El Emprendimiento Tech C.A.",
    "FiscalRegistry": "J297182748",
    "Address": "GUATIRE CC BUENAVENTURA",
    "Phone": "2123310585",
    "EmailTo": "elcorreprincipal@email.com",
    "EmailCc": "elcorredecopia@email.com",
    "PaymentType": "Credito 1 mes",
    
    //Moneda principal en la cual se emite el documento
    "Currency": "VES",

    //Totales del documento expresados en la moneda principal
    "PreviousBalance": 0,
    "Discount": 0,
    "ExemptAmount": 0,
    "TaxBase": 501.72,
    "TaxPercent": 16,
    "TaxAmount": 80.2752,
    "Total": 581.9952,
    "IGTFBaseAmount": 0,
    "IGTFPercentage": 3,
    "IGTFAmount": 0,
    "GrandTotal": 581.9952,
    "AmountLetters": "XXXXX 01/100",

    //Moneda conversion 
    "ConversionCurrency": "VES",
    //Tasa de cambio oficial utilizada para la conversión
    "ExchangeRate": 1,

    //Totales del documento expresados en la moneda conversión
    "PreviousBalanceVES": 0,
    "DiscountVES": 0,
    "ExemptAmountVES": 0,
    "TaxBaseVES": 501.72,
    "TaxPercentVES": 16,
    "TaxAmountVES": 80.2752,
    "TotalVES": 581.9952,
    "IGTFBaseAmountVES": 0,
    "IGTFAmountVES": 0,
    "GrandTotalVES": 581.9952,
    "AmountLettersVES": "XXXXX 01/100",

    //Campos complementarios
    "BCVMessage": "Es un mensaje que hace referencia a ciertos articulos",
    "SystemReference": "",
    "CreditNoteText": "", //Un mensaje explicativo de que factura esta siendo afectada por la nota de credito, preferiblemente se debe usar el campo "AffectedDocumentNumber", "AffectedDocumentType", "AffectedDocumentSerie" ya que este campo sera deprecado.
    "Note1": "",
    "Note2": "",
    "Note3": "",
    "Sucursal": "", //Oficina 
    "Seller": "", 
    "PaymentCurrency": "",
    "City": "",
    "AccountNumber":"",
    "CustomerIdentificationType":"RIFCEDULA",
    "TemplateName":"",
    "AffectedDocumentNumber":0, //Si el documento es una nota de crédito, en este campo se especifica el numero de factura afectada
    "AffectedDocumentType":"NN", //El tipo de documento afectado FA => Factura, ND => Nota de Debito, NC => Nota de Credito, en el 99% se debe establecer a FA.
    "AffectedDocumentSerie":"0", //Si la empresa tiene varias series, aqui se debe escribir la serie a la cual esta siendo afectada, en el 99% de los casos solo existe una serie, en este campo NO se escribe el StrongId (GUID) de la Serie, sino su LETRA, ejemplo A, B, C.
    "CustomerCategory":"",
    "DocumentCategory":"",
    "Extra": {},

    "Details": [
        {
            "Description": "Mi Producto A",
            "Quantity": 1,
            "UnitPrice": 301.72, //Precio de venta unitario sin impuesto
            "Amount": 301.72, //Resultado de multiplicar Quantity * Amount
            "TaxPercent": 16, //Porcentaje del Impuesto que aplique al producto
            "TaxAmount": 48.2752, //Monto del impuesto, al aplicar el campo TaxPercent contra Amount
            "IsExempt": 0, //Indica si el producto o servicio esta Exento o no
            "UnitPriceVES":0, //Precio de venta unitario sin impuesto en la moneda de conversion
            "AmountVES":0,  //Resultado de multiplicar Quantity * Amount en la moneda de conversion
            "TaxAmountVES":0, //Monto del impuesto, al aplicar el campo TaxPercent contra Amount en la moneda de conversion
            "OperationCode": "C001"
        },
{
            "Description": "Mi Producto B",
            "Quantity": 2,
            "UnitPrice": 100, //Precio de venta unitario sin impuesto
            "Amount": 200, //Resultado de multiplicar Quantity * Amount
            "TaxPercent": 16, //Porcentaje del Impuesto que aplique al producto
            "TaxAmount": 32, //Monto del impuesto, al aplicar el campo TaxPercent contra Amount
            "IsExempt": 0, //Indica si el producto o servicio esta Exento o no
            "UnitPriceVES":0, //Precio de venta unitario sin impuesto en la moneda de conversion
            "AmountVES":0,  //Resultado de multiplicar Quantity * Amount en la moneda de conversion
            "TaxAmountVES":0, //Monto del impuesto, al aplicar el campo TaxPercent contra Amount en la moneda de conversion
            "OperationCode": "C002"
        }        
    ]
}
```

### Por lotes (hasta 500 documentos por solicitud)

La intención de crear documentos por lotes es utilizar eficientemente los recursos enviando solicitudes con un arreglo de documentos asociados a un ciclo.

El ciclo es un componente que permite agrupar N cantidad de documentos, de hecho incluso al crear un documento (facturación en línea) también se crea un ciclo pero con un solo documento asociado.

Los ciclos deben seguir una ruta para que los documentos queden "Asignados" o "Cancelados".

El algortimo recomendado sería el siguiente:

```code
Abrir Ciclo
Hacer Mientras existan documentos
    Agregar documentos al Ciclo
    Si ocurrio un Error agregando Documentos al Ciclo
        Cerrar Ciclo 
        Cancelar Ciclo 
        Fin del Programa
    Fin Si
Fin del Hacer
Cerrar Ciclo
Aprobar Ciclo
Fin del Programa
```

#### Notas
* Por defecto la cantidad de documentos que soporta el request es de 100, si desea ampliar 500 debe solicitarlo.

#### Paso 1: Abrir ciclo

Crea un ciclo dentro del API, y retorna el StrongId (GUID) que representa al Ciclo, este dato es importante y sera utilizado posteriormente para ejecutar acciones sobre el ciclo, y también para agregar documentos.

POST /batch/open

```javascript
{
    "SerieStrongId":"be375781-4b32-41fd-ae28-cf82a35d7a50"
}
```

#### Paso 2: Agregar documentos al ciclo

La intención es indicar hasta un maximo de 500 documentos para agregar a un ciclo o lote determinado.

Solo se pueden agregar documentos a Ciclos que esten actualmente con estatus Abierto.


POST /documents/create

```javascript
{
    "BatchStrongId": "21b386d9-92b4-499a-976a-babb61057714",
    "Docs":[
        {
            "SerieStrongId": "a1b9deb6-11a9-4e4f-8d33-a349db640d8c",
            "DocumentType": "ND",
            "Number": 99875427,
            "EmissionDateAndTime": "7/6/2022 15:28",

            //Datos del contribuyente a quien se le crea el documento 
            "Name": "El Emprendimiento Tech C.A.",
            "FiscalRegistry": "J297182748",
            "Address": "GUATIRE CC BUENAVENTURA",
            "Phone": "2123310585",
            "EmailTo": "elcorreprincipal@email.com",
            "EmailCc": "elcorredecopia@email.com",
            "PaymentType": "Credito 1 mes",
            
            //Moneda principal en la cual se emite el documento
            "Currency": "USD",

            //Totales del documento expresados en la moneda principal
            "PreviousBalance": 0,
            "Discount": 0,
            "ExemptAmount": 100,
            "TaxBase": 100,
            "TaxPercent": 16,
            "TaxAmount": 16,
            "Total": 216,
            "IGTFBaseAmount": 216,
            "IGTFPercentage": 3,
            "IGTFAmount": 6.48,
            "GrandTotal": 222.48,
            "AmountLetters": "Un mil seiscientos diez con 01/100",

            //Moneda conversion 
            "ConversionCurrency": "VES",
            //Tasa de cambio oficial utilizada para la conversión
            "ExchangeRate": 25,

            //Totales del documento expresados en la moneda conversión
            "PreviousBalanceVES": 0,
            "DiscountVES": 0,
            "ExemptAmountVES": 1125.48,
            "TaxBaseVES": 1125.48,
            "TaxPercentVES": 16,
            "TaxAmountVES": 180.07,
            "TotalVES": 2431.03,
            "IGTFBaseAmountVES": 2431.03,
            "IGTFAmountVES": 72.9309,
            "GrandTotalVES": 2503.9609,
            "AmountLettersVES": "Un mil seiscientos diez con 01/100",

            //Campos complementarios
            "BCVMessage": "Es un mensaje que hace referencia a ciertos articulos",
            "SystemReference": "",
            "CreditNoteText": "",
            "Note1": "",
            "Note2": "",
            "Note3": "",
            "Sucursal": "",
            "Seller": "",
            "PaymentCurrency": "",
            "City": "",
            "AccountNumber":"",
            "CustomerIdentificationType":"RIFCEDULA",
            "TemplateName":"",
            "AffectedDocumentNumber":0,
            "AffectedDocumentType":"NN",
            "AffectedDocumentSerie":"0",
            "CustomerCategory":"",
            "DocumentCategory":"",
            "Extra": {},

            "Details": [
                {
                    "Description": "Poliza de Prima X",
                    "Quantity": 1,
                    "UnitPrice": 301.72,
                    "Amount": 301.72,
                    "TaxAmount": 48.28,
                    "TaxPercent": 16,
                    "IsExempt": 0,
                    "UnitPriceVES":0,
                    "AmountVES":0,
                    "TaxAmountVES":0,
                    "OperationCode": "C001"
                }
            ]
        }
    ]
}
```

#### Paso 3: Cerrar ciclo

Al cerrar un ciclo ya no podra agregar documentos, y solo podrá Aprobarlo o Cancelarlo.

POST /batch/close

```javascript
{
    "Id":"60bf868f-112c-49e8-9f20-587d0f7c9905"
}
```

#### Aprobar ciclo

Al Aprobar un ciclo le esta indicando al Sistema que esta conforme con su contenido y que el ciclo queda disponible para la asignación de los números de control y conversión del documento a documento fiscal.

POST /batch/approve

```javascript
{
    "Id":"cf0a20ab-4bed-40fd-aa4a-034eddf3c9ac"
}
```

#### Cancelar ciclo

Al Cancelar un ciclo le esta indicando al Sistema que rechazas el contenido del ciclo y que este no debe afectar las numeraciones, libros de ventas, etc. El ciclo quedará disponible para ser eliminado en cualquier momento.

POST /batch/cancel

```javascript
{
    "Id":"cd216bbb-01df-4264-b02b-bee11d7f5a6a",
    "Errors":""
}
```

##### Campos obligatorios

- `Id` 


### En línea y Pre-asignado

La creación de un documento en línea pre-asignado implica la creación de un ciclo, agregar un documento e inmediatamente aprobarlo y marcarlo como asignado ya que es la Empresa quien realiza la asignacion del numero de control NO la imprenta. Este método simula en una sola llamada la creación del ciclo, agregar documento, cerrar el ciclo y aprobarlo. Por eso es altamente recomendable para facturación en vivo o en línea.

Antes de armar el JSON para enviar al API se deben aplicar unas funciones de resumen y luego cifrar el resultado, los campos que necesitan este procedimiento son EncryptedHashCompany y EncryptedHashPrintingHouse.

Para obtener el Hash se aplica el siguiente algoritmo

```
//C#
//Solo deben ser usados los montos en la moneda principal de la factura que sera
//el mismo establecido en el campo Currency del JSON

var hash = new StringBuilder();
hash.Append(ControlNumbersBookStrongId);
hash.Append(ControlNumber);
hash.Append(SerieStrongId);
hash.Append(DocumentType);
hash.Append(Number);
hash.Append(FiscalRegistry);
hash.Append(MainCurrency);
hash.Append(Discount.VES);
hash.Append(ExemptAmount.GetCurrency(MainCurrency));
hash.Append(TaxBaseZero.GetCurrency(MainCurrency));
hash.Append(TaxPercentZero);
hash.Append(TaxAmountZero.GetCurrency(MainCurrency));
hash.Append(TaxBase.GetCurrency(MainCurrency));
hash.Append(TaxPercent);
hash.Append(TaxAmount.GetCurrency(MainCurrency));
hash.Append(Total.GetCurrency(MainCurrency));
hash.Append(IGTFBaseAmount.GetCurrency(MainCurrency));
hash.Append(IGTFPercentage);
hash.Append(IGTFAmount.GetCurrency(MainCurrency));
hash.Append(GrandTotal.GetCurrency(MainCurrency));

var encryptedHashCompany = funcion_cifrado_rsa(hash, llave_privada_empresa);
var encryptedHashPrintingHouse = funcion_cifrado_rsa(hash, llave_publica_imprenta);

```

POST /documents/createpreassigned

```javascript
{
    "EncryptedHashCompany": "",
    "EncryptedHashPrintingHouse":"",
    "ControlNumbersBookStrongId":"",
    "ControlNumber":1,
    "SerieStrongId": "a03d6609-755d-4cf3-b1f7-1815fd62aaaf",
    "DocumentType": "FA",
    "Number": 16876433,
    "EmissionDateAndTime": "12/31/2022 15:28:28",
    "FiscalRegistry": "J297182748",
    "MainCurrency": "VES",
    "ConversionCurrency":"USD",
    "Discount": { "VES": 0, "USD": 0 },
    "ExemptAmount": { "VES": 0, "USD": 0 },
    "TaxBaseZero": { "VES": 0, "USD": 0 },
    "TaxPercentZero": 0,
    "TaxAmountZero": { "VES": 0, "USD": 0 },
    "TaxBase": { "VES": 0, "USD": 0 },
    "TaxPercent": 0,
    "TaxAmount": { "VES": 0, "USD": 0 },
    "Total": { "VES": 0, "USD": 0 },
    "IGTFBaseAmount":  { "VES": 0, "USD": 0 },
    "IGTFAmount":  { "VES": 0, "USD": 0 },
    "IGTFPercentage": 3,
    "GrandTotal":  { "VES": 0, "USD": 0 },
    "ExchangeRate":  { "VES": 0, "USD": 0 },     
    "City":"",
    "CustomerIdentificationType" : "RIFCEDULA",
    "Extra": {},
    //Si el documento es una Nota de Credito
    "AffectedDocumentNumber":0,
    "AffectedDocumentType":"NN",
    "AffectedDocumentSerie":"0" 
}

```

***
## Otras operaciones

### Ver información de un documento asignado

Si deseas saber el número de control asignado y número de documento (solo aplica en algunos casos) puedes llamar al siguiente endpoint pasando como parametro el GUID recibido al momento de crear el documento.

POST|GET  /documents/info/{strongid}

#### Respuesta exitosa

```javascript
{
    "result": {
        "strongId": "aaddff1f-2f67-4654-9af7-42cd75954b80",
        "id": 2694954,
        "serie": "0",
        "documenType": "FA",
        "number": 1,
        "controlNumber": 10,
        "emissionDate": "2023-08-09T13:35:45.3113258",
        "name": "Pepe Lepu",
        "fiscalRegistry": "V11111111",
        "address": "En Algun Lugar",
        "phone": "+584242202020",
        "emailTo": "pepelepu@pepelepu.com",
        "emailCc": "pepelepu@pepelepu.com",
        "paymentType": "Pago inmediato",
        "currency": "VES",
        "previousBalance": 0.000000,
        "discount": 0.000000,
        "exemptAmount": 0.000000,
        "taxBase": 107.420000,
        "taxPercent": 16.000000,
        "taxAmount": 17.180000,
        "total": 124.600000,
        "igtfBaseAmount": 124.600000,
        "igtfPercentage": 0.000000,
        "igtfAmount": 124.600000,
        "grandTotal": 249.200000,
        "amountLetters": "CIENTO SIETE CON 42/100",
        "conversionCurrency": "VES",
        "exchangeRate": 0.000000,
        "previousBalanceVES": 0.000000,
        "discountVES": 0.000000,
        "exemptAmountVES": 0.000000,
        "taxBaseVES": 0.000000,
        "taxPercentVES": 16.000000,
        "taxAmountVES": 0.000000,
        "totalVES": 0.000000,
        "igtfBaseAmountVES": 0.000000,
        "igtfAmountVES": 0.000000,
        "grandTotalVES": 0.000000,
        "amountLettersVES": "CERO",
        "bcvMessage": "En los casos en que la base imponible de la venta o prestaci?n de serviciosestuviere expresado en moneda extranjera, se establecer? la equivalencia enmoneda nacional, al tipo de cambio corriente en el mercado del d?a en que ocurrael hecho imponible. (Art. 25 LEY IVA, 38 Y 52 Regl Ley IVA). Los pagos estipuladosen moneda extranjera se deben cancelar salvo convenci?n especial, con la entregade lo equivalente en moneda de curso legal (Bs), al tipo de cambio oficial.",
        "systemReference": "1",
        "creditNoteText": null,
        "note1": null,
        "note2": null,
        "note3": null,
        "sucursal": null,
        "seller": null,
        "paymentCurrency": null,
        "city": null,
        "accountNumber": null,
        "customerIdentificationType": {
            "id": 1,
            "description": "RIFCEDULA",
            "label": "RIF / C.I."
        },
        "affectedDocumentNumber": 0,
        "affectedDocumentType": "NN",
        "affectedDocumentSerie": "0",
        "customerCategory": null,
        "documentCategory": null,
        "details": [
            {
                "operationCode": "1",
                "description": "Un producto facturado para pepelepu",
                "quantity": 1.000000,
                "unitPrice": 7.760000,
                "amount": 7.760000,
                "taxAmount": 16.000000,
                "taxPercent": 0.000000,
                "isExempt": false
            }
        ]
    },
    "errors": [],
    "success": [],
    "information": [],
    "haserrors": false
}
```



***
## Tutoriales

### Crear documento en dólares con conversión en bolívares

### Crear documento en bolívares con conversión en dólares

### Buscar documento por numero de control

POST /documents/searchbycontrolnumber

```javascript

{
    "Number":1
}

```

#### Respuesta exitosa

```javascript

{
    "result": [
        {
            "strongId": "36ebb1ca-99a6-4961-94c0-e9919c65ed76",
            "control": 1,
            "number": 1,
            "emissionDate": "2022-12-28T00:00:00",
            "documentType": "FA",
            "name": "MILENA CARDOZO",
            "total": 19.900000,
            "currency": "VES",
            "annulled": false
        }
    ],
    "errors": [],
    "success": [],
    "information": [],
    "haserrors": false
}

```

### Buscar documento por numero de documento y serie

POST /documents/searchbynumberandserie

```javascript

 {
    "Number": 1,
    "Serie":"A",
    "DocumentType":"FA"
}

```

#### Respuesta exitosa

```javascript
{
    "result": [
        {
            "strongId": "36ebb1ca-99a6-4961-94c0-e9919c65ed76",
            "control": 1,
            "number": 1,
            "emissionDate": "2022-12-28T00:00:00",
            "documenType": "FA",
            "name": "MILENA CARDOZO",
            "total": 19.900000,
            "currency": "VES",
            "annulled": false
        }
    ],
    "errors": [],
    "success": [],
    "information": [],
    "haserrors": false
}
```


### Visualizar un documento por URL

### Obtener el stream en PDF de un documento


